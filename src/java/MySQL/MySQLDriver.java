package MySQL;

import java.sql.*;
import java.util.Properties;

public class MySQLDriver {
	private static final String dbClassName = "com.mysql.jdbc.Driver";
	private static final String CONNECTION = "jdbc:mysql://127.0.0.1/data";

	private static Connection getConnection(){
		try {
			Class.forName(dbClassName);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
	    Properties p = new Properties();
	    p.put("user","data");
	    p.put("password","password");
	    
	    Connection c;
		try {
			c = DriverManager.getConnection(CONNECTION,p);
			return c; 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null; 
        }
}