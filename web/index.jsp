<%-- 
    Document   : index
    Created on : Sep 12, 2017, 9:20:04 PM
    Author     : recap
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registration</title>
    </head>
    <body>
        <h1>Registered</h1>

        <sql:setDataSource var="db" driver="com.mysql.jdbc.Driver"
        url="jdbc:mysql://localhost/omahahuskyrescue"
        user="root" password="root"/>

        <c:if test="${pageContext.request.method=='POST'}">
        <c:catch var="exception">

        <sql:update dataSource="${db}" var="updatedTable">
            INSERT INTO omahahuskyrescue.Registration (firstname,lastname,email,phone) VALUES (?,?,?,?)
            <sql:param value="${param.firstname}" />
            <sql:param value="${param.lastname}"/>
            <sql:param value="${param.email}"/>
            <sql:param value="${param.phone}"/>
        </sql:update>

        <c:if test="${updatedTable>=0}">
        <font> Congratulations! You account was created.</font>

        </c:if>
        </c:catch>
        <c:if test="${exception!=null}">
        <c:out value="Unable to insert data in database." />
        <c:out value="${exception}" />
        </c:if>
        </c:if> 
        
        <br>
        Click <a href="index.html"> here </a> to return back to homepage.

    </body>
</html>